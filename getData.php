<?php 
//Variablendefinitionen
$type = "Planet";
$planet = "Mercury";
$latitude = 48.935786;
$longitude = 9.205312;
if(isset ($_GET["object"]))		//Ausnahmen überprüfen
{
	$planet = $_GET["object"];
	if ($planet == "Pluto")
	{
		 $type = "Dwarf%20Planet";
	}
	if ($planet == "Moon") 
	{
		$type = "Satellite";
	}
}

if(isset ($_GET["latitude"]) && isset ($_GET["longitude"]))		//Erhalten der Positionsdaten
{
	$latitude = $_GET["latitude"];
	$longitude = $_GET["longitude"];
	echo ("Location Set to latitude: " . $latitude . " and longitude: " . $longitude . "\n");
}

$url = 'https://ssp.imcce.fr/webservices/miriade/api/ephemcc.php?-name=' . $planet . '&-type=' . $type . '&-ep=2459332.9408973497&-tscale=UTC&-nbd=1&-step=1d&-rplane=1&-theory=INPOP&-teph=2&-observer=' . $longitude . '%20' . $latitude . '%200.0&-output=--iofile(ssp-ephemcc-observation.xml)&-lang=fr&-from=ssp';
  
$file_name = 'XMLdata.txt';

if(file_put_contents( $file_name,file_get_contents($url))) {		//Herunterladen des XML-Files
    echo "File downloaded successfully\n,";
}
else {
    echo "File downloading failed.";
}
//Extrahieren der Daten aus dem XML-File
$str = file_get_contents($url);
$arr = explode('<vot:TR>',$str); //vorne abschneiden
$arr = explode('</vot:TR>',$arr[1]); //hinten abschneiden
$arr = explode('<vot:TD>',$arr[0]); //einzelne Werte trennen
$Rek = explode('</vot:TD>', $arr[2]);
$Dek = explode('</vot:TD>', $arr[3]);
$Az = explode('</vot:TD>', $arr[5]);
$H = explode('</vot:TD>', $arr[6]);
$m = explode('</vot:TD>', $arr[11]);
if (10 < $H[0] && $H[0] < 170)
{
	$Sichtbarkeit = "Ja";
} else {
	$Sichtbarkeit = "Nein";
}
//Rückgabe der Informationen als String
echo('Azimut: '. $Az[0]. "°,". 'Höhenwinkel: ' . $H[0] . "°,". 'Scheinbare Helligkeit: ' . $m[0] . 'mag,Sichtbarkeit: ' . $Sichtbarkeit . ',,Latitude: ' . $latitude . ',Longitude: ' . $longitude . ",");
file_put_contents('Data.txt', $Az[0]. "\n". $H[0] . "\n". $m[0]);
?>
