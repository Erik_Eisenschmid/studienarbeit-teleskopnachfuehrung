<!DOCTYPE html>
<html lang="en-US">
<head>
<style>
<! -- Styling der Anzeige -->
*{
	box-sizing: border-box;
}
body{
	font-family: verdana;
}
header{
	background-color: #666;
	padding: 1px;
	text-align: center;
	font-size: 25px;
	color: white;
}
section{
	display: -webkit-flex;
	display: flex;
	background: #ccc;
}
section ul {
	list-style-type: none;
	padding: 10px;
}
article{
	-webkit-flex: 3;
	-ms-flex: 3;
	flex: 3;
	background-color: #f1f1f1;
	padding: 20px;
}
footer {
	background-color: #777;
	padding: 10px;
	text-align: center;
	color: white;
}
img {
	float: left;
}
@media (max-width: 600px) {
	section ul {
		-webkit-flex-direction: column;
		flex-direction: column;
	}
}
</style>
</head>

<script>
//Setzen von Variablen
var latitude = 0;
var longitude = 0;
var foto_shown = 0;
var i = 0;
var currentDate = new Date().toLocaleString();
var planets = ["Moon", "Mercury", "Venus", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune", "Pluto"];

function sendRequest(planet) { //Anfrage 
	foto_shown = 0;
	currentDate = new Date().toLocaleString();
	var xhr = new XMLHttpRequest();
	timestamp = 0;
	if (navigator.geolocation){
		navigator.geolocation.getCurrentPosition(function(position){		//Feststellung der Geolocation
		latitude = position.coords.latitude;
		longitude = position.coords.longitude;
		xhr.open("GET", '/Backend.php?action=request&object='+planet+'&latitude='+latitude+'&longitude='+longitude);
		xhr.send();
		xhr.onload = function() {
			console.log('backend wurde aufgerufen')
			console.log(xhr.response);
			var res = xhr.response.split(",");
			console.log(res);
			var target = document.getElementById(planet);
			target.innerHTML ="<br/>" + "zuletzt aktualisiert: " + currentDate + "<br/>" + "<br/>" + "<br/>" + res[1] + "<br/>" + res[2] + "<br/>" + res[3] + "<br/>" + res[4] + "<br/>" + res[5] + "<br/>" + res[6]+ "<br/>" + res[7];
			timestamp = res[9];
			console.log("T"+timestamp);
		}
		}
		, null,{enableHighAccuracy:true});
			console.log(latitude, longitude);
		}else{
			x.innerHTsML = "Geolocation ist not supported by this Browser.";
		}
		
		Interval = setInterval(function loop() { 	//Intervallanfrage zur Abfrage ob das erwartete Bild bereits existiert
			Foto = planet+"/"+planet+"Foto"+timestamp+".jpg";
			if (foto_shown == 0) {
				if(checkFileExist(Foto) == 1){		//Anzeige des Bildes
					var img = document.createElement("img");
					img.id = planet+"IMAGE";
					img.src = Foto;
					img.height = "384";
					img.width = "512";
					console.log(document.getElementById(planet+"IMAGE"));
					
					if(document.getElementById(planet+"IMAGE") != null){
						document.getElementById(planet+"Picture").innerHTML = " ";
						document.getElementById(planet+"Picture").appendChild(img);
					}else{
						document.getElementById(planet+"Picture").appendChild(img);
					}

					foto_shown = 1;
					clearInterval(Interval);
				}
			};
		}, 5000);
}

function checkFileExist(urlToFile) {	//Abfrage ob eine Datei existiert
    var xhr = new XMLHttpRequest();
    xhr.open('HEAD', urlToFile, false);
    xhr.send();
    if (xhr.status == "404") {
        return false;
    } else {
        return true;
    }
}

function loadData(i, callback){		//Anfrage der Planetendaten inkl. Rückmeldung 
	planet = planets[i];
	console.log(planet);
	currentDate = new Date().toLocaleString();
	xhr = new XMLHttpRequest();
	if (navigator.geolocation){
		navigator.geolocation.getCurrentPosition(function(position){	
			latitude = position.coords.latitude;
			longitude = position.coords.longitude;
			console.log("Backendaufruf"+planet);
			xhr.open("GET", '/Backend.php?action=getData&object='+planet+'&latitude='+latitude+'&longitude='+longitude);
			xhr.send();
			xhr.onload = callback;
		},null,{enableHighAccuracy:true});
	}
}


Interval_refresh = setInterval(function getData(){		//Intervallabfrage zur Aktualisierung der Planetendaten
	if(i < planets.length){
		loadData(i, function() {		//Aufteilen und Anzeigen der Planetendaten
			console.log('aktualisiere Daten:')
			console.log(xhr.response);
			var res = xhr.response.split(",");
			var target = document.getElementById(planet);
			target.innerHTML ="<br/>" + "zuletzt aktualisiert: " + currentDate + "<br/>" + "<br/>" + "<br/>" + res[1] + "<br/>" + res[2] + "<br/>" + res[3] + "<br/>" + res[4] + "<br/>" + res[5] + "<br/>" + res[6]+ "<br/>" + res[7];
			i++;
		});
		console.log(latitude, longitude);
	} else {
		i = 0;
	}
}, 5000);

</script>

<body>
<! -- Aufbau des HTML Dokuments -->
<h1 style="background-color:white; text-align:center;">Webserver: Teleskop mit Nachführung</h1>
<header>
	<h2>Objekte</h2>
</header>
<section>
        <ul>
                <h2>Mond</h2>
		<img type="image" src="Mond.jpeg" alt="Mond" onclick="sendRequest('Moon')">
		</a>
        </ul>
        <ul>
		 <div id="Moon"></div>
        </ul>
	<ul>
		 <div id="MoonPicture"></div>
        </ul>
</section>
<section>
	<ul>
		<h2>Merkur</h2>
                <img type="image" src="Merkur.jpeg" alt="Merkur" onclick="sendRequest('Mercury')">
		</a>
	</ul>
	<ul>
                 <div id="Mercury"></div>
	</ul>
	<ul>
		<div id="MercuryPicture"></div>
        </ul>
</section>
<section>
        <ul>
                <h2>Venus</h2>
                <img type="image" src="Venus.jpeg" alt="Venus" onclick="sendRequest('Venus')">
		</a>
        </ul>
        <ul>
                 <div id="Venus"></div>
        </ul>
	<ul>
		<div id="VenusPicture"></div>
        </ul>
</section>
<section>
        <ul>
                <h2>Mars</h2>
                <img type="image" src="Mars.jpeg" alt="Mars" onclick="sendRequest('Mars')">
		</a>
        </ul>
        <ul>
                 <div id="Mars"></div>
        </ul>
	<ul>
		<div id="MarsPicture"></div>
        </ul>
</section>
<section>
        <ul>
                <h2>Jupiter</h2>
                <img type="image" src="Jupiter.jpeg" alt="Jupiter" onclick="sendRequest('Jupiter')">
		</a>
        </ul>
        <ul>
                 <div id="Jupiter"></div>
        </ul>
	<ul>
		<div id="JupiterPicture"></div>
        </ul>
</section>
<section>
        <ul>
                <h2>Saturn</h2>
                <img type="image" src="Saturn.jpeg" alt="Saturn" onclick="sendRequest('Saturn')">
		</a>
        </ul>
        <ul>
                 <div id="Saturn"></div>
        </ul>
	<ul>
		<div id="SaturnPicture"></div>
        </ul>
</section>
<section>
        <ul>
                <h2>Uranus</h2>
                <img type="image" src="Uranus.jpeg" alt="Uranus" onclick="sendRequest('Uranus')">
		</a>
        </ul>
        <ul>
                 <div id="Uranus"></div>
        </ul>
	<ul>
		<div id="UranusPicture"></div>
        </ul>
</section>
<section>
        <ul>
                <h2>Neptun</h2>
                <img type="image" src="Neptun.jpeg" alt="Neptun" onclick="sendRequest('Neptune')">
		</a>
        </ul>
        <ul>
                 <div id="Neptune"></div>
        </ul>
	<ul>
		<div id="NeptunePicture"></div>
        </ul>
</section>
<section>
        <ul>
                <h2>Pluto</h2>
                <img type="image" src="Pluto.jpeg" alt="Pluto" onclick="sendRequest('Pluto')">
		</a>
        </ul>
        <ul>
                 <div id="Pluto"></div>
        </ul>
	<ul>
		<div id="PlutoPicture"></div>
        </ul>
</section>

<footer>
	<p></p>
</footer>
<?php
?>
</body>
</html>
