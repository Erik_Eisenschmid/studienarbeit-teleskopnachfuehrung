#!/usr/bin/python3

import time #importieren von Bibliotheken
import sys
import cv2
import os
from datetime import datetime
import board
import RPi.GPIO as GPIO

from adafruit_motorkit import MotorKit
from adafruit_motor import stepper
kit = MotorKit(i2c=board.I2C())

import cgitb
cgitb.enable()

# Pin setup der Magnetsensoren
GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.setup(24, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)

# Kamera Setup und Einstellungen
cam = cv2.VideoCapture(-1)

cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)

time.sleep(2)

cam.set(cv2.CAP_PROP_EXPOSURE, 8.0) #negativ entspricht höherer Belichtungszeit
cam.set(cv2.CAP_PROP_BRIGHTNESS, 0) #0 ist neutral, 128 heller, -128 dunkler
cam.set(cv2.CAP_PROP_SATURATION, 256) #0 (s/w) bis 512, 256 ist gut
cam.set(cv2.CAP_PROP_CONTRAST, 32) #0 schlecht bis 32?
cam.set(cv2.CAP_PROP_SHARPNESS, 0) #sharpness 0 nicht schlecht
cam.set(cv2.CAP_PROP_GAIN, 32) #0 schlechter als 32/64 aber 1000 auch nicht gut

def main():
	n=1
	x = len(sys.argv)		
	while n <= x:		#Daten erhalten
		print ("arg%d= %s" %(n, sys.argv[n]))	
		if (n == 1): Rektaszension = sys.argv[n]
		if (n == 2): Deklination = sys.argv[n]
		if (n == 3): objekt = sys.argv[n]
		n=n+1
		if (n == 4): break
	
	#------------------------------------------------------------------------------------------------Referenzierung
	#start links vom Magnetsensor
	while (1):
		kit.stepper2.onestep(direction=stepper.FORWARD, style=stepper.SINGLE)
		if GPIO.input(23) == GPIO.HIGH: 
			print("Deklinationsachse Nullpunkt gesetzt")
			CurrentDek = 0 
			break
	
	while (1):
		kit.stepper1.onestep(direction=stepper.FORWARD, style=stepper.SINGLE)
		if GPIO.input(24) == GPIO.HIGH: 
			print("Rektaszensionsachse Nullpunkt gesetzt")
			CurrentRek = 0 
			break
			
	# Breitengrad = 48,6819° ~ 48°40m55s
	
	#------------------------------------------------------------------------------------------------Ausrichtung
	# Vorrübergehende Faktoren zur Umrechnung
	Faktor = 2448
	Zeitfaktor = 4,6 
	Dauer_Ausrichtung = Zeitfaktor/1000 * (abs(Rektaszension-CurrentRek) + abs(Deklination - CurrentDek)) * Faktor #Erwartete Dauer zum Ausrichten -> muss nachgeführt werden
		
	if (Rektaszension > CurrentRek):
		for i in range((Rektaszension - CurrentRek) * Faktor)
			#Nach links 
			kit.stepper1.onestep(direction=stepper.BACKWARD, style=stepper.SINGLE)
			time.sleep(0.002)
	
	if (Rektaszension < CurrentRek):
		for j in range((CurrentRek - Rektaszension) * Faktor)
			#Nach rechts
			kit.stepper1.onestep(direction=stepper.FORWARD, style=stepper.SINGLE)
			time.sleep(0.002)
	
	if (Deklination > CurrentDek):
		for k in range((Deklination - CurrentDek) * Faktor)
			#Nach oben
			kit.stepper2.onestep(direction=stepper.BACKWARD, style=stepper.SINGLE)
			time.sleep(0.002)
		
	if (Deklination < CurrentDek):
		for l in range((CurrentDek - Deklination) * Faktor)
			#Nach unten
			kit.stepper2.onestep(direction=stepper.FORWARD, style=stepper.SINGLE)
			time.sleep(0.002)
		
	#------------------------------------------------------------------------------------------------Nachführung
	# Eigene Nachführgeschwindigkeit des Mondes bei Bedarf
	if (objekt == Moon):
		sleep = 0.108
	else:
		sleep = 0.105
		
	for i in range(Dauer_Ausrichtung / Zeitfaktor * 1000 * 1,1) #Korrektur Ausrichtdauer, mal 1,1 da bei elf mal so schneller Bewegung 
		NF1 = kit.stepper1.onestep(direction=stepper.FORWARD, style=stepper.SINGLE)
		time.sleep(0.00909*sleep) #11 mal so schnell wie die "normale Nachführung"
	
	while(1):		# normale Nachführung
		NF2 = kit.stepper1.onestep(direction=stepper.FORWARD, style=stepper.SINGLE)
		time.sleep(sleep)
	
	CurrentRek = Rektaszension + (NF1+NF2)/Faktor # Derzeitige Ausrichtung des Teleskops 
	CurrentDek = Deklination
		
	#------------------------------------------------------------------------------------------------Aufnahme
	
	secondsSinceEpoch = time.time()
	timeObj = time.localtime(secondsSinceEpoch)
	timestamp = ',%d-%d-%d %d:%d:%d' % (timeObj.tm_mday, timeObj.tm_mon, timeObj.tm_year, timeObj.tm_hour, timeObj.tm_min, timeObj.tm_sec)
	print (',%d-%d-%d %d:%d:%d' % (timeObj.tm_mday, timeObj.tm_mon, timeObj.tm_year, timeObj.tm_hour, timeObj.tm_min, timeObj.tm_sec))
	
	#Archivierung des Bildes
	directory = "/var/www/html/{}".format(objekt)
	os.chdir(directory)
	re, img = cam.read()
	cv2.imwrite('{}Foto{}.jpg'.format(objekt, timestamp), img)

if __name__=="__main__":
	main()
	
