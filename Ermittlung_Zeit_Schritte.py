import time		#importieren von Bibliotheken
import sys
import os
from datetime import datetime
import board
import RPi.GPIO as GPIO

from adafruit_motorkit import MotorKit
from adafruit_motor import stepper
kit = MotorKit(i2c=board.I2C())

import cgitb
cgitb.enable()

# Pin setup der Magnetsensoren
GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.setup(24, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)

# Code zur Feststellung der Umrechnungsfaktoren zwischen Einzelschritten und Winkeln im äquatorialen Koordinatensystem
while (GPIO.input(23) == GPIO.LOW):
		kit.stepper2.onestep(direction=stepper.BACKWARD, style=stepper.SINGLE)
		time.sleep(0.003)
		if (GPIO.input(23) == GPIO.HIGH):		#Sensor wird zum ersten mal angefahren -> Start der Messungen
			print("Start")
			start_time = time.time()
			break

while (GPIO.input(23) == GPIO.HIGH):
		MidSteps = kit.stepper2.onestep(direction=stepper.BACKWARD, style=stepper.SINGLE)
		time.sleep(0.003)
		if (GPIO.input(23) == GPIO.LOW):		#Sensor wird verlassen
			print(MidSteps)		# Zwischenwert speichern
			break

while (GPIO.input(23) == GPIO.LOW):
		EndSteps = kit.stepper2.onestep(direction=stepper.BACKWARD, style=stepper.SINGLE)
		time.sleep(0.003)
		if (GPIO.input(23) == GPIO.HIGH):		#Sensor wird zum zweiten mal angefahren
			print(EndSteps)
			break
#Ausgabe der Ergebnisse
print("GESAMTSTEPS %s" % (MidSteps + EndSteps))
print("ZEIT %s" % (time.time()-start_time))
print("Anfahrbare Schritte %s" % ((MidSteps + EndSteps)/16))
print("Schritte pro Grad %s" % ((MidSteps + EndSteps)/16/360))
print("Dauer pro 1000 Schritte %s" % (((time.time()-start_time)/(MidSteps + EndSteps)/16)*1000))
