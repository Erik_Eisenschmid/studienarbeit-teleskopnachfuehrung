----------------#Die folgende Datei wurde vor allem zum Testen von Code und Dokumentieren von Erkenntnissen während der Versuchsdurchführung verwendet 
----------------#und wird vollständigkeitshalber mit angehängt

"Test for using adafruit_motorkit with a stepper"

#stepper1 = Terminals M1 und M2 | stepper2 = Terminals M3 und M4

----------------#Erste Messungen mit den Motoren

#kit.stepper1.onestep(direction=stepper.BACKWARD, style=stepper.SINGLE)
#kit.stepper1.onestep(direction=stepper.BACKWARD, style=stepper.DOUBLE)
#kit.stepper1.onestep(direction=stepper.BACKWARD, style=stepper.INTERLEAVE)
#kit.stepper1.onestep(direction=stepper.BACKWARD, style=stepper.MICROSTEP)

"für i = 1 entspricht 16 Schritte"	
"i = 5800 entspricht einer ganzen Umdrehung am Getriebeausgang des Motors"	
"i = 3200000 entspricht 90 Grad Drehung am Teleskop in ca. 27min. 36sek."	

import time
import board
import RPi.GPIO as GPIO

from adafruit_motorkit import MotorKit
from adafruit_motor import stepper

GPIO.setmode(GPIO.BCM)
GPIO.setup(23, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
GPIO.setup(24, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)
kit = MotorKit(i2c=board.I2C())

----------------# Testen der Magnetsensoren

"""
i=0
while 1:
        if GPIO.input(23) == GPIO.HIGH:
            print("Eingang High")
            i = i+1
        else:
            print("Eingang Low")
"""


------------------# Code zur Feststellung der Nachführgeschwindigkeiten (Verwendung bei der Versuchsdurchführung)

"""
for i in range(2000):
    print(kit.stepper1.onestep(direction=stepper.FORWARD, style=stepper.SINGLE))
    time.sleep(0.108)
    "Ausrichtung Deklinationsachse"
    
    "print(kit.stepper1.onestep(direction=stepper.BACKWARD, style=stepper.SINGLE))" 
    "Ausrichtung Deklinationsachse"
    
    "print(kit.stepper2.onestep(direction=stepper.FORWARD, style=stepper.SINGLE))"
    "Stepper2 Forward = runter, Stepper2 Backward = hoch"
    "Nachführung Rektaszensionsachse"
    
    print(kit.stepper2.onestep(direction=stepper.FORWARD, style=stepper.SINGLE))
    "Nachführung Rektaszensionsachse "
    
    "Lunaregeschwindigkeit 0.108s sleeptime bei onestep"
    "Saturngeschwindigkeit 0.105s sleeptime bei onestep"
"""
    
----------------# Dauernachführung

"""for i in range(200000):
    print(kit.stepper1.onestep(direction=stepper.BACKWARD, style=stepper.SINGLE))  
    time.sleep(0.002)"""

----------------# Stromlos schalten der Motoren

kit.stepper1.release()
kit.stepper2.release()
